import React from 'react';

import './index.scss';

const App = () => {
    return (
        <div className="app-main">
            <div className="app-header">Header</div>
            <div className="app-content">Content</div>
            <div className="app-footer">Footer</div>
        </div>
    );
}

export default App;