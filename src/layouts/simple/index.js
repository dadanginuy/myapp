import React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import { Header, Footer, PageNotFound } from './components';

import routers from '../../routers/public';

import './index.scss';

const App = () => {

    return (
        <div className="app-main">
            <BrowserRouter>
                <Header />
                <div className="app-content">
                    <Switch>
                        {/* <Route path="/home" component={React.lazy(() => import('../../views/Home'))} /> */}
                        {/* <Route path="/about" component={React.lazy(() => import('../../views/About'))} /> */}
                        {/* <Route path="/about" component={React.lazy(() => import('../../views/About'))} /> */}
                        {routers.map((item,idx)=>{
                            return <Route key={idx} {...item}/>
                        })}
                        <Redirect exact from="/" to="/home" />
                        <Route><PageNotFound /></Route>
                    </Switch>
                </div>
            </BrowserRouter>
            <Footer />
        </div>
    );
}

export default App;