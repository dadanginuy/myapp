import React from 'react';

import menus from '../../../routers/menus';
import { useHistory } from 'react-router';

const App = () => {
    const history = useHistory();

    const handleOnClick = (item) => {
        history.push(item.to);
    }

    return (
        <div className="app-header">
            <div className="menu-list">
                {menus.map((menu,idx) => (
                    <div key={idx} className="menu-item" onClick={() => { handleOnClick(menu) }}>{menu.text}</div>
                ))}
            </div>
        </div>
    );
}

export default App;