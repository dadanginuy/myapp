import Header from './Header';
import Footer from './Footer';
import PageNotFound from './NotFound';

export { Header, Footer, PageNotFound };