import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';

const Landing = React.lazy(() => import('./layouts/simple'));

const App = () => (
  <div>
    <Suspense fallback={<div>loading...</div>}>
      <Landing />
    </Suspense>
  </div>
)

ReactDOM.render(<App />, document.getElementById('root'));
