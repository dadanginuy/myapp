import React, { useEffect, useState } from 'react';
import axios from 'axios';
import './index.style.css';
import CardPost from './components/CardPost';

const App = (props) => {
    const history = props.history;
    const [post, setPost] = useState([]);

    const getPost = async () => {
        const response = await axios.get('https://jsonplaceholder.typicode.com/posts');
        if(response.data){
            setPost(response.data);
        }
        //eksekusi dibawah;
        console.log('response',response);
    }

    const handleDelete = async (id) => {
        const response = await axios.delete(`https://jsonplaceholder.typicode.com/posts/${id}`);
        if (response.data) {
            console.log('response.data',response);
            getPost();
        }
    }

    useEffect(()=>{
        getPost();
    },[]);

    return (
        <div className='home-container'>
           <div className='title-home'>Halaman Post</div>
           <button onClick={()=>history.push('/page1')}>Ke Page 1</button>
           <div className='post-container'>
                {post.map((m,idx)=>{
                    return <CardPost key={idx} data={m} onDelete={handleDelete}/>
                })}
           </div>
        </div>
    )
}

export default App