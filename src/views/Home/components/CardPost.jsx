import React from 'react'

const CardPost = (props) => {
    const { data, onDelete } = props;

    return (
        <div className='card-post'>
            <h4>{data.title}</h4>
            <p>{data.body}</p>
            <button onClick={()=>onDelete(data.id)}>HAPUS</button>
        </div>
    )
}

export default CardPost