const list = [
    { text: 'Home', to: '/home' },
    { text: 'Page 01', to: '/page1' },
    { text: 'Page 02', to: '/page2' },
    { text: 'Page 03', to: '/page3' },
    { text: 'About', to: '/about' },
]

export default list;