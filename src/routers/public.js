import React from 'react';

const list = [
    { path: '/home', exact: true, component: React.lazy(() => import('../views/Home')) },
    { path: '/page1', exact: true, component: React.lazy(() => import('../views/Page1')) },
    { path: '/about', exact: true, component: React.lazy(() => import('../views/About')) },
]

export default list;